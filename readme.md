# EC2 Instance Provisioning

# Configure Vagrant
1. get a Snack! 
2. add vagrant aws plugin
```
vagrant plugin install vagrant-aws
```
3. add vagrant dummy box
```
vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
```
3. configure your vagrant file for AWS (deactivate smb sync)
```
config.vm.synced_folder ".", "/vagrant", disabled: true
```
4. configure vm parameters and insert access_key + id
```
aws.access_key_id = "XXXXXXXXXXXXXXXXXXXXXXXXX" 
    aws.secret_access_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" 
    aws.keypair_name = "AWS Instance 2" 
    aws.ami = "ami-0d542ef84ec55d71c" #Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type
    aws.region = "us-east-2" 
    aws.instance_type = "t2.micro" 
```
5. start vagrant EC2 instance
```
vagrant up --provider=aws
```
